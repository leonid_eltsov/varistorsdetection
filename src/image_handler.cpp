#include "image_handler.h"

#include <iostream>
#include <algorithm>

//#include <opencv2/imgcodecs.hpp>
//#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "minIni.h"

using namespace cv;
using namespace std;

ImageHandler::ImageHandler(int width , int height, string filename):width_px(width), height_px(height), IniFileName(filename)
{
    ReadSettings();
    RecalcLayoutGeometry();
}


ImageHandler::~ImageHandler()
{
    WriteSettings();
}


void ImageHandler::ProcessImage (Mat& img)
{
    vector<Vec3f> circles_px = FindCircles(img);

    DrawLayout(img);
    DrawCircles(img, circles_px);
    DrawArrow(img);

    static WorkStates WorkState = Detecting;
    static int idle_attempts_cnt = 0;
    static vector< vector<int> > FoundDiameters;

    switch (WorkState)
    {
    case Detecting:

        FindProperVaristor(circles_px);

        if (x_mm < 0) idle_attempts_cnt++;
        else idle_attempts_cnt = 0;

        if (idle_attempts_cnt >= IDLE_ATTEMPT_MAX_CNT)
        {
            varistor_diameter_mm = varistor_diameter_min;
            varistor_diameter_delta_mm = varistor_diameter_delta_mm_default;

            idle_attempts_cnt = 0;

            FoundDiameters.clear();
            FoundDiameters.resize(2);

            WorkState = DiameterAutodetection;
        }

        break;
    case DiameterAutodetection:

        putText(img, "Diameter Autodetection mode", Point(200, 200), FONT_HERSHEY_PLAIN, 1, Scalar(255, 0, 0));

        FoundDiameters.at(0).push_back(varistor_diameter_mm);
        FoundDiameters.at(1).push_back(circles_px.size());

        varistor_diameter_mm++;

        if(varistor_diameter_mm >= varistor_diameter_max)
        {
            auto it = max_element(FoundDiameters.at(1).begin(), FoundDiameters.at(1).end());

            varistor_diameter_mm = FoundDiameters.at(0).at( distance(FoundDiameters.at(1).begin(), it) );

            if (varistor_diameter_mm == varistor_diameter_min) varistor_diameter_mm = varistor_diameter_mm_default;

            WorkState = Detecting;
        }

        RecalcLayoutGeometry();

        break;
    default:
        WorkState = Detecting;
        break;
    }
}


void ImageHandler::ReadSettings (void)
{
    minIni ini(IniFileName);

    param_1 = ini.geti("Algorithm", "param_1", param_1_default);
    param_2 = ini.geti("Algorithm", "param_2", param_2_default);

    k_cal = ini.getf("Algorithm", "k_cal", k_cal_default);
    b_cal = ini.geti("Algorithm", "b_cal", b_cal_default);

    x_pos_error_mm = ini.geti("Algorithm", "x_pos_error_mm", x_pos_error_mm_default);
    y_pos_error_mm = ini.geti("Algorithm", "y_pos_error_mm", y_pos_error_mm_default);

    center_line_px = ini.geti("Geometry", "center_line_px", center_line_px_default);

    active_rect.x     = ini.geti("Geometry", "active_area_left_bound_px", active_rect_x_default);
    active_rect.width = ini.geti("Geometry", "active_area_width_px", active_rect_width_default);

    active_area_width_mm = ini.geti("Geometry", "active_area_width_mm", active_area_width_mm_default);

    varistor_diameter_mm = ini.geti("Algorithm", "varistor_diameter_mm", varistor_diameter_mm_default);
    varistor_diameter_delta_mm    = ini.geti("Algorithm", "diameter_delta_mm", varistor_diameter_delta_mm_default);
}


void ImageHandler::WriteSettings (void) const
{
    minIni ini(IniFileName);

    bool b = true;

    b &= ini.put("Algorithm", "param_1", to_string(param_1));
    b &= ini.put("Algorithm", "param_2", to_string(param_2));

    char str[32];
    sprintf(str, "%1.3f", k_cal);   b &= ini.put("Algorithm", "k_cal", str);

    b &= ini.put("Algorithm", "b_cal",                      to_string(b_cal));
    b &= ini.put("Algorithm", "x_pos_error_mm",             to_string(x_pos_error_mm));
    b &= ini.put("Algorithm", "y_pos_error_mm",             to_string(y_pos_error_mm));
    b &= ini.put("Algorithm", "varistor_diameter_mm",       to_string(varistor_diameter_mm));
    b &= ini.put("Algorithm", "diameter_delta_mm",          to_string(varistor_diameter_delta_mm));
    b &= ini.put("Geometry", "center_line_px",              to_string(center_line_px));
    b &= ini.put("Geometry", "active_area_left_bound_px",   to_string(active_rect.x));
    b &= ini.put("Geometry", "active_area_width_px",        to_string(active_rect.width));
    b &= ini.put("Geometry", "active_area_width_mm",        to_string(active_area_width_mm));

    assert(b);
}

float ImageHandler::Average (float new_val) const
{
    static float ret = -1;
    static vector<float> vec(frames_cnt);

    if (vec.size() == 0)      vec.push_back(new_val);
    else
    {
        float avrg = accumulate(vec.begin(), vec.end(), 0.0)/vec.size();

        if ( abs(new_val - avrg) < x_pos_error_mm )   vec.push_back(new_val);
        else
        {
            ret = -1;
            vec.clear();
        }

        if (vec.size() >= frames_cnt)
        {
            ret = avrg;
            vec.clear();
        }
    }

    return ret;
}


void ImageHandler::DrawLayout (Mat& img) const
{
    int active_area_center_x = active_rect.x + (active_rect.width)/2;
    int active_area_center_y = active_rect.y + (active_rect.height)/2;

    circle(img, Point( active_area_center_x, active_area_center_y), R_min_px, Scalar(0, 255, 0), 0.5, LINE_AA, 0);
    circle(img, Point( active_area_center_x, active_area_center_y), R_max_px, Scalar(0, 255, 0), 0.5, LINE_AA, 0);

    putText(img, " Diameter: (" + to_string(varistor_diameter_mm) + " +/- " + to_string(varistor_diameter_delta_mm) + ") mm", Point(active_area_center_x, active_rect.y + R_max_px*2 + 15), FONT_HERSHEY_PLAIN, 1, Scalar(0, 0, 255, 255));

    line(img, Point(active_rect.x, active_area_center_y),   Point(active_rect.x + active_rect.width, active_area_center_y), Scalar(0, 255, 0), 1, LINE_AA, 0);
    rectangle(img, active_rect, Scalar(0, 255, 0), 2, LINE_AA, 0);

    char str[16];
    sprintf(str, "k_cal = %1.3f", k_cal);
    putText(img, str, Point(20 , 20), FONT_HERSHEY_PLAIN, 1, Scalar(0, 0, 255, 255));

    putText(img, "b_cal = " + to_string(b_cal),                     Point(20, 40), FONT_HERSHEY_PLAIN, 1, Scalar(0, 0, 255, 255));
    putText(img, "x_err = " + to_string(x_pos_error_mm) + " mm",    Point(200, 20), FONT_HERSHEY_PLAIN, 1, Scalar(0, 0, 255, 255));
    putText(img, "y_err = " + to_string(y_pos_error_mm) + " mm",    Point(200, 40), FONT_HERSHEY_PLAIN, 1, Scalar(0, 0, 255, 255));


    for(int i = 0; i <= 10; ++i)
    {
        int mark = round( active_rect.width*(i/10.0) );
        line(img, Point(active_rect.x + mark, active_rect.y), Point(active_rect.x + mark , active_rect.y - 20), Scalar(0, 255, 0), 1, LINE_AA, 0);
        putText(img, to_string( (int)round(mark*px2mm) ),   Point(active_rect.x + mark , active_rect.y - 20), FONT_HERSHEY_PLAIN, 1, Scalar(0, 0, 255, 255));
    }

}

void ImageHandler::DrawCircles (Mat& img, const vector<Vec3f>& circles_px) const
{
    for (auto const &c_px : circles_px)
    {
        circle(img, Point(c_px[0] , c_px[1]), c_px[2], Scalar(0, 0, 255), 0.5, LINE_AA);
        circle(img, Point(c_px[0] , c_px[1]),      2 , Scalar(0, 255, 0), 0.5, LINE_AA);

        int x,y,r;
        Circle_px2mm(c_px, x, y, r);

        putText(img, to_string(x) + ","+ to_string(y) + "," + to_string(2*r), Point(c_px[0] - R_px, c_px[1]), FONT_HERSHEY_PLAIN, 1, Scalar(0, 0, 255, 255));
    }
}

void ImageHandler::DrawArrow (Mat& img) const
{
    if(x_mm <= 0 ) return;

    int arrow_length = 50;
    arrowedLine(img, Point(x_px, active_rect.y + active_rect.height + arrow_length + 40), Point(x_px, active_rect.y + active_rect.height + 40), Scalar(255, 0, 0), 2, 8, 0, 0.1);
    putText(img, to_string(x_mm) + " mm (" + to_string(x_px) + " px)", Point(x_px + 10, active_rect.y + arrow_length + 2*R_max_px ), FONT_HERSHEY_PLAIN, 1, Scalar( 255, 0, 0));
}

vector<Vec3f> ImageHandler::FindCircles (const Mat& img) const
{
    Mat sub_img = img(active_rect);
    Mat edges;
    Mat grey;
    vector<Vec3f> circles_px;

    cvtColor(sub_img, edges, COLOR_BGR2GRAY);
    GaussianBlur(edges, grey, Size(7, 7), 1.5, 1.5);
    Canny(grey, edges, 0, 30, 3);

    HoughCircles(edges, circles_px, HOUGH_GRADIENT, 1,
                 1.75*R_px,                             // change this value to detect circles with different distances to each other
                 param_1, param_2,                      // change the last two parameters
                 R_min_px, R_max_px                     // (min_radius & max_radius) to detect larger circles
                 );

    for (auto &c_px : circles_px)
    {
        c_px[0] += active_rect.x;
        c_px[1] += active_rect.y;
    }

    return circles_px;
}


void ImageHandler::Circle_px2mm(const Vec3f& circle_px, int &x_mm, int &y_mm, int &r_mm) const
{
    x_mm = (circle_px[0] - active_rect.x)*px2mm;
    y_mm = (circle_px[1] - active_rect.y)*px2mm;
    r_mm = (circle_px[2])*px2mm;
}

void ImageHandler::RecalcLayoutGeometry (void)
{
    if(varistor_diameter_mm < varistor_diameter_min) varistor_diameter_mm = varistor_diameter_min;
    if(varistor_diameter_mm > varistor_diameter_max) varistor_diameter_mm = varistor_diameter_max;
    if( (varistor_diameter_delta_mm < 0)||(varistor_diameter_delta_mm > varistor_diameter_mm/2) ) varistor_diameter_delta_mm = varistor_diameter_delta_mm_default;


    px2mm = active_area_width_mm/(float)(active_rect.width);

    R_px = varistor_diameter_mm/(2*px2mm);
    R_min_px = (varistor_diameter_mm - varistor_diameter_delta_mm)/(2*px2mm);
    R_max_px = (varistor_diameter_mm + varistor_diameter_delta_mm)/(2*px2mm);

    active_rect.y = center_line_px - R_max_px;
    active_rect.height = R_max_px*2;

    if ( (active_rect.x < 0)||(active_rect.width < 0)||(active_rect.y < 0)||(active_rect.height < 0)||
         (active_rect.x + active_rect.width > width_px)||(active_rect.y + active_rect.height > height_px) )
    {
        active_rect.x = active_rect_x_default;
        active_rect.width = active_rect_width_default;
        center_line_px = center_line_px_default;

        active_rect.y = center_line_px - R_max_px;
        active_rect.height = R_max_px*2;
    }
}

void ImageHandler::ProcessKey (int key)
{
    if (key == -1) return; // No key

    switch(key)
    {
    case '+':   varistor_diameter_mm++;         break;
    case '-':   varistor_diameter_mm--;         break;
    case '*':   varistor_diameter_delta_mm++;   break;
    case '/':   varistor_diameter_delta_mm--;   break;
    case 'q':   active_rect.x--;                break;
    case 'w':   active_rect.x++;                break;
    case 'a':   active_rect.width--;            break;
    case 's':   active_rect.width++;            break;
    case 'e':   center_line_px--;               break;
    case 'd':   center_line_px++;               break;
    case 'r':   param_1++;                      break;
    case 'f':   param_1--;                      break;
    case 't':   param_2++;                      break;
    case 'g':   param_2--;                      break;
    case 'k':   b_cal--;                        break;
    case 'l':   b_cal++;                        break;
    case 'i':   k_cal-=0.001;                   break;
    case 'o':   k_cal+=0.001;                   break;
    default:                                    break;
    }

    RecalcLayoutGeometry();
    WriteSettings();
}

void ImageHandler::FindProperVaristor (vector<Vec3f> &circles_px)
{
    if (circles_px.size() == 0)
    {
        x_mm = -1;
    }

    sort(circles_px.begin(), circles_px.end(), [](Vec3f pt1, Vec3f pt2) { return (pt1[0] < pt2[0]); });

    for (auto const &c_px : circles_px)
    {
        int x_c,y_c,r_c;
        Circle_px2mm(c_px, x_c, y_c, r_c);

        int center_line_dist_mm = abs( y_c - (( active_rect.height/2.0)*px2mm) ) ;

        if (center_line_dist_mm < y_pos_error_mm)
        {
            int x = round( Average(x_c) );

            if (x > 0)
            {
                x_px = c_px[0];
                x_mm = x*k_cal + b_cal;;
            }
            else
            {
                x_px = -1;
                x_mm = -1;
            }
            break;
        }
    }
}

int ImageHandler::GetPosition() const
{
    return x_mm;
}
