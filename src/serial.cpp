#include "serial.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include <iostream>
#include <cerrno>
#include <cstring>
#include <chrono>

Serial::Serial(const char* port, int baudrate)
    :m_thread()
{
    m_stop_thread = false;
    Open(port, baudrate);
}

Serial::~Serial()
{
    m_stop_thread = true;
    if(m_thread.joinable()) m_thread.join();

    Close();
}

int Serial::Open (const char* portname, int speed)
{
    speed = GetBaudrate(speed);

    m_fd = open (portname, O_RDWR | O_NOCTTY | O_SYNC);

    if (m_fd < 0)
    {
        std::cerr<<"error opening "<<std::strerror(errno)<<portname<<std::endl;
        return -1;
    }

    SetInterfaceAttribs(speed, 0);
    SetBlocking(0);                 // set no blocking

    return 1;
}

void Serial::Close (void)
{
    close(m_fd);
}

void Serial::Start()
{
    // This will start the thread. Notice move semantics!
    m_thread = std::thread(&Serial::WorkerThread, this);
}

void Serial::WorkerThread()
{
    while(!m_stop_thread)
    {
        Process();
        std::this_thread::sleep_for( std::chrono::milliseconds(100) );
    }
}

void Serial::Process()
{
    char request[BUF_SIZE];
    char response[BUF_SIZE];

    memset(&request[0], 0, sizeof(request) );

    int n = read(m_fd, request, sizeof(request) );

    m_mutex.lock();
    strcpy(response, m_response);
    m_mutex.unlock();

    if (n > 0)
    {
        if( request[0] == 'p' )	write (m_fd, response, strlen(response) );
    }
}

void Serial::SetResponse(const char* resp)
{
    m_mutex.lock();
    strcpy(m_response, resp);
    m_mutex.unlock();
}

void Serial::SetResponse(std::string resp)
{
    m_mutex.lock();
    strcpy(m_response, resp.c_str());
    m_mutex.unlock();
}

int Serial::SetInterfaceAttribs (int speed, int parity)
{
    struct termios tty;

    memset (&tty, 0, sizeof tty);

    if (tcgetattr (m_fd, &tty) != 0)
    {
        std::cerr<<"error from tcsetattr "<<std::strerror(errno)<<std::endl;
        return -1;
    }

    cfsetospeed (&tty, speed);
    cfsetispeed (&tty, speed);

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
    // disable IGNBRK for mismatched speed tests; otherwise receive break
    // as \000 chars
    tty.c_iflag &= ~IGNBRK;         // disable break processing
    tty.c_lflag = 0;                // no signaling chars, no echo,
    // no canonical processing
    tty.c_oflag = 0;                // no remapping, no delays
    tty.c_cc[VMIN]  = 0;            // read doesn't block
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
    // enable reading
    tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
    tty.c_cflag |= parity;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;

    if (tcsetattr (m_fd, TCSANOW, &tty) != 0)
    {
        std::cerr<<"error from tcsetattr "<<std::strerror(errno)<<std::endl;
        return -1;
    }

    return 0;
}

void Serial::SetBlocking (int should_block)
{
    struct termios tty;

    memset (&tty, 0, sizeof tty);
    if (tcgetattr (m_fd, &tty) != 0)
    {
        std::cerr<<"error from tggetattr "<<std::strerror(errno)<<std::endl;
        return;
    }

    tty.c_cc[VMIN]  = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

    if (tcsetattr (m_fd, TCSANOW, &tty) != 0)
    {
        std::cerr<<"error setting term attributes "<<std::strerror(errno)<<std::endl;
    }
}


int Serial::GetBaudrate(int baudrate)
{
    switch (baudrate)
    {
    case 9600:
        return B9600;
    case 19200:
        return B19200;
    case 38400:
        return B38400;
    case 57600:
        return B57600;
    case 115200:
        return B115200;
    case 230400:
        return B230400;
    case 460800:
        return B460800;
    case 500000:
        return B500000;
    case 576000:
        return B576000;
    case 921600:
        return B921600;
    case 1000000:
        return B1000000;
    case 1152000:
        return B1152000;
    case 1500000:
        return B1500000;
    case 2000000:
        return B2000000;
    case 2500000:
        return B2500000;
    case 3000000:
        return B3000000;
    case 3500000:
        return B3500000;
    case 4000000:
        return B4000000;
    default:
        return -1;
    }
}
