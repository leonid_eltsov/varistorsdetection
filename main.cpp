#include "image_handler.h"
#include "serial.h"

#include <iostream>
#include <opencv2/highgui/highgui.hpp>

const int ESC_KEY = 27;
const int DELAY_WAITKEY = 10; // ms

int main(int argc, char **argv)
{	


    Serial serial("/dev/ttyUSB0", 115200);
    serial.Start();

    int width_px  = 1280  /* 1280 800 640*/;
    int height_px = 720  /* 720   600 480*/;

    cv::Mat frame;

    cv::VideoCapture cap(0);    // open the default camera
    cap.set(cv::CAP_PROP_FRAME_WIDTH,  width_px);
    cap.set(cv::CAP_PROP_FRAME_HEIGHT, height_px);

    if (!cap.isOpened())        // check if we succeeded
        return -1;

    cap >> frame; // get a new frame from camera

    cv::namedWindow("frame", cv::WINDOW_FULLSCREEN);
    cv::setWindowProperty("frame", cv::WND_PROP_FULLSCREEN, cv::WINDOW_FULLSCREEN);

    std::cout << "Resolution: " << frame.cols << "X" << frame.rows << std::endl;

    width_px = frame.cols;
    height_px = frame.rows;

    ImageHandler *ImgHandler = new ImageHandler( width_px, height_px, "settings.ini");

    while(1)
    {
        cap >> frame; // get a new frame from camera

        ImgHandler->ProcessImage(frame);
        imshow("frame", frame);

        int x = ImgHandler->GetPosition();
        if (x > 0)  serial.SetResponse(std::to_string(x));

        int key = cv::waitKey(DELAY_WAITKEY);
        if (key == ESC_KEY) break;
        ImgHandler->ProcessKey(key);
    }
    
    delete ImgHandler;
    // the camera will be deinitialized automatically in VideoCapture destructor
    return 0;
}

