#ifndef SERIAL_H
#define SERIAL_H

#include <thread>
#include <mutex>

#define BUF_SIZE		16

class Serial
{
private:

    std::thread m_thread;
    std::mutex m_mutex;

    bool m_stop_thread;

    char m_response[BUF_SIZE];
    int m_fd;

    int		Open (const char* portname, int speed);
    void	Close (void);

    int  	SetInterfaceAttribs (int speed, int parity);
    void 	SetBlocking (int should_block);
    int 	GetBaudrate (int baudrate);

    void WorkerThread();
    void Process (void);

public:


    Serial(const char* portname = "/dev/ttyUSB0", int speed = 9600);
    ~Serial();

    void Start();
    void SetResponse(const char* resp);
    void SetResponse(std::string resp);

};


#endif
