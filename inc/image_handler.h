#ifndef IMAGE_HANDLER_H
#define IMAGE_HANDLER_H

#include <opencv2/core/core.hpp>
#include <string>

class ImageHandler
{
private:

    int width_px;
    int height_px;

    const int param_1_default = 100;
    const int param_2_default = 26;
    const float k_cal_default = 1;
    const int b_cal_default = 0;
    const int x_pos_error_mm_default = 3;
    const int y_pos_error_mm_default = 3;

    const int center_line_px_default = 300;
    const int active_rect_x_default = 10;
    const int active_rect_width_default = 300;
    const int active_area_width_mm_default = 500;
    const int varistor_diameter_mm_default = 30;
    const int varistor_diameter_delta_mm_default = 8;
    const int varistor_diameter_min = 25;
    const int varistor_diameter_max = 100;

    int param_1 = param_1_default;
    int param_2 = param_2_default;

    float k_cal = k_cal_default;
    int b_cal = b_cal_default;

    int x_pos_error_mm = x_pos_error_mm_default;
    int y_pos_error_mm = y_pos_error_mm_default;

    int center_line_px = center_line_px_default;

    int varistor_diameter_mm = varistor_diameter_mm_default;
    int varistor_diameter_delta_mm = varistor_diameter_delta_mm_default;

    int active_area_width_mm = active_area_width_mm_default;

    int frames_cnt = 10;

    float px2mm;


    int R_px;
    int R_min_px;
    int R_max_px;

    cv::Rect active_rect;

    int x_px;
    int x_mm;


    const int IDLE_ATTEMPT_MAX_CNT = 100;

    enum WorkStates {Detecting, DiameterAutodetection};

    std::string IniFileName;

    void WriteSettings (void) const;
    void ReadSettings (void);

    void RecalcLayoutGeometry (void);
    void DrawLayout (cv::Mat& img) const;
    void DrawCircles (cv::Mat& img, const std::vector<cv::Vec3f>& circles) const;
    void DrawArrow (cv::Mat& img) const;

    std::vector<cv::Vec3f> FindCircles (const cv::Mat& img) const;
    void Circle_px2mm(const cv::Vec3f& circle_px, int &x_mm, int &y_mm, int &r_mm) const;
    float Average (float new_val) const;
    void FindProperVaristor (std::vector<cv::Vec3f> &circles);


public:

    ImageHandler(int width = 640, int height = 480, std::string filename = "settings.ini");

    void ProcessImage (cv::Mat& img);
    void ProcessKey (int key);
    int  GetPosition(void) const;

    ~ImageHandler();
};


#endif
